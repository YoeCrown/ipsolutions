<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
        return view('welcome');
})->name('home');

Route::prefix('{locale}/')->middleware('set_lang')->group(function () {

    Route::get('/', function () {
        return view('welcome');
    })->name('home');

    Route::get('servicio-internet', function(){
        return view('servicio-internet');
    })->name('internet');

    Route::get('servicio-voip', function(){
        return view('servicio-voip');
    })->name('voip');

    Route::get('nosotros', function() {
        return view('nosotros');
    })->name('about');
    
    Route::get('empleo', function() {
        return view('empleo');
    })->name('job');


    Route::get('soporte', function(){
        return view('soporte');
    })->name('suport');

    Route::resource('servicio', 'ServiceController');


    Route::get('/home', 'HomeController@index');
    Auth::routes();

});


