@extends('layouts.app')

@section('content')
  
    <menu-vista></menu-vista>

    <div class="container">
      <div id="demo">
        
        <center>
            <h1>{{trans('textos.voip_title')}}</h1>
            <h2>{{trans('textos.voip_subtitle')}}</h2>
        </center>   
        <div class="table-responsive-vertical shadow-z-1">

            <table id="table" class="table table-hover table-mc-light-blue striped">
                <thead>
                    <tr>
                    <th><b>PLAN</b></th>
                    <th><b>{{trans('textos.voip_price')}}</b></th>
                    <th><b>{{trans('textos.voip_support')}} 24/7</b></th>
                    <th><b>{{trans('textos.voip_contract')}}</b></th>
                    </tr>
                </thead>
                <tbody>
                    
                    <tr>
                    <td data-title="ID">{{trans('textos.voip_residential')}}</td>
                    <td data-title="Precio"><b>{{trans('textos.voip_residential_price')}}</b></td>
                    <td data-title="Status"><span style="font-size:11px"><b>{{trans('textos.voip_dias')}}</b><br>{{trans('textos.voip_via')}}</span></td>
                    <td data-title="Acction"> <modal-service tiposervicio="voip residencial"></modal-service></td>
                    </tr>
                    
                    <tr>
                    <td data-title="ID">{{trans('textos.voip_measured')}}</td>
                    <td data-title="Precio"><b>{{{trans('textos.voip_measured_price')}}}</b></td>
                    <td data-title="Status"><span style="font-size:11px"><b>{{trans('textos.voip_dias')}}</b><br>{{trans('textos.voip_via')}}</span></td>
                    <td data-title="Acction"><modal-service tiposervicio="voip measured"></modal-service></td>
                    </tr>
                    
                    <tr>
                    <td data-title="ID">{{trans('textos.voip_commercial')}}</td>
                    <td data-title="Precio"><b>{{trans('textos.voip_commercial_price')}}</b></td>
                    <td data-title="Status"><span style="font-size:11px"><b>{{trans('textos.voip_dias')}}</b><br>{{trans('textos.voip_via')}}</span></td>
                    <td data-title="Acction"><modal-service tiposervicio="voip comercial"></modal-service></td>
                    </tr>

                    <tr>
                    <td data-title="ID">{{trans('textos.voip_regular')}}</td>
                    <td data-title="Precio"><b>{{trans('textos.voip_regular_price')}}<br> {{trans('textos.voip_regular_price_b')}}</b></td>
                    <td data-title="Status"><span style="font-size:11px"><b>{{trans('textos.voip_dias')}}</b><br>{{trans('textos.voip_via')}}</span></td>
                    <td data-title="Acction"><modal-service tiposervicio="voip regular"></modal-service></td>
                    </tr>
            
                </tbody>
            </table>
        </div>
      </div> 
    </div>
    <footer-me></footer-me>

@endsection
