@extends('layouts.app')

@section('content')

<menu-vista></menu-vista>
<section> 
   <div class="container">
    <div class="row" style="margin-top:4em">
      <div class="col l4">
        <img src="{{asset('images/support/promo.png')}}" alt="">
      </div>
      <div class="col l8" style="border-left: 1px solid  #f5f5f5; ">
        <div class="row" style="border-bottom: 1px solid  #f5f5f5; ">
          <div class="col l2">
            <img src="{{asset('images/support/pago.png')}}" alt="" style="margin-top:2em;">
          </div>
          <div class="col l10" >
            <h5>{{trans('textos.support_title_questions')}}</h5>
            <p>{{trans('textos.support_questions_content')}}</p>
          </div>
        </div>
        <div class="row" style="border-bottom: 1px solid  #f5f5f5; ">
          <div class="col l2">
            <a href="http://support.ip-solutions.us/" target="__blank">
              <img src="{{asset('images/support/problemas.png')}}" alt="" style="margin-top:2em;">
            </a>
          </div>
          <div class="col l10">
            <a href="http://support.ip-solutions.us/" target="__blank"> 
              <h5>{{trans('textos.support_title_inconvenient')}}</h5>
            </a> 
            <p>{{trans('textos.support_inconvenient_content')}}</p>
          </div>
        </div>
        <div class="row" style="border-bottom: 1px solid  #f5f5f5; ">
          <div class="col l2">
            <a href="https://ip-solutions.us/servicio-de-geolocalizacion-gratis" target="__blank">
              <img src="{{asset('images/support/coordenadas.png')}}" alt="" style="margin-top:2em;">
            </a>
          </div>
          <div class="col l10">
            <a href="https://ip-solutions.us/servicio-de-geolocalizacion-gratis" target="__blank">
              <h5>{{trans('textos.support_title_coorde')}}</h5>
            </a>  
            <p>{{trans('textos.support_coorde_content')}}</p>
          </div>
        </div>
        <div class="row" style="border-bottom: 1px solid  #f5f5f5; ">
          <div class="col l2">
            <a href="http://ip-solutions.us/calcular" target="__blank">
              <img src="{{asset('images/support/calculadora.png')}}" alt="" style="margin-top:2em;">
            </a>  
          </div>
          <div class="col l10">
            <a href="http://ip-solutions.us/calcular" target="__blank">
             <h5>{{trans('textos.support_title_calculate')}}</h5>
            </a>   
            <p>En caso que requieras conocer cuanto internet requieres en tu casa, esta es tu herramienta ideal, aqui verificaras
               cuanto internet necesitas para tu hogar y lograr el equilibrio y estabilidad al usar servicios de play estation, hulu, roku,
               apple tv, netflix ,etc.</p>
          </div>
        </div>

      </div>
    </div>
   </div>

</section>

<footer-me></footer-me>
@endsection


