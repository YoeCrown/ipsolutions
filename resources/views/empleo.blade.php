@extends('layouts.app')

@section('content')
<menu-vista></menu-vista>

<div class="row jobs" id="jobs">
    <h4 class="center">OPORTUNIDADES DE EMPLEO</h4>
</div>

<div class="row">
    <div class="container"> <br>    
        <p>Somos una empresa en constante crecimiento en el area tecnologico , donde brindamos excelente servicios de alta calidad, por la cual siempre estamos en la busque de personal capacitado para seguir
        brindando nuestros servicios y cubriendo aun de manera eficaz el mercado,es  por ello que estaremos contento de recibirte en nuestra familia IP-SOLUTIONS </p><br>
    </div>
</div>
      <div class="divider"></div>

<div class="row">
    <h4 style="margin-left:2em">Nuestros Departamentos</h4>
</div>
<div class="row">
    <div class="col s12 m6">
     <center>
        <img class="responsive-img"src="{{asset('images/empleo.png')}}" alt="" width="400em" style="border-radius:1em;margin-bottom:2em;">
     </center>
    </div>
    <div class="col s12 m6"> 
        <tabs></tabs>
    </div>
</div>
  <div class="divider"></div>

<div class="row">
    <h4 style="margin-left:2em">Beneficios</h4>
</div>

<div class="container">
    <p>El trabajo y la dedicación de nuestros talentos son la fuerza detrás de nuestro éxito, y continuamente buscamos manera de recompensarlos.  Nuestra estrategia de  compensación y reconocimiento está diseñada para atraer, motivar y retener una fuerza trabajadora comprometida, a la vez que promovemos el logro sostenido de nuestros objetivos de negocio.  Proveemos a los empleados con elementos de compensación que satisfacen sus metas personales y laborales.</p>
    <div class="row">
            <div class="col s3 offset-s3">
                    <ul>
                        <li><i class="material-icons">start</i> Medico</li>
                        <li><i class="material-icons">start</i> Bono</li>
                        <li><i class="material-icons">start</i> Alimenticio</li>
                        <li><i class="material-icons">start</i> Seguro</li>
                        <li><i class="material-icons">start</i> Discapacidad</li>
                    </ul>
            </div>
            <div class="col s5 offset-s1">
                    <ul>
                        <li><i class="material-icons">start</i> Medico</li>
                        <li><i class="material-icons">start</i> Bono</li>
                        <li><i class="material-icons">start</i> Alimenticio</li>
                        <li><i class="material-icons">start</i> Seguro</li>
                        <li><i class="material-icons">start</i> Discapacidad</li>
                    </ul>
            </div>
    </div>
</div>
<div class="divider"></div>
<div class="row">
    <h4 style="margin-left:2em">Interesados</h4>
</div>
<div class="row">
    <div class="container"> <br>    
        <div class="col s6">
            <p>Si estas interesado y quieres ser parte del equipo envianos tu resumen y cuentanos un poco en que te destacas y que puedes brindarle a la empresa. </p><br>
        </div>
        <div class="col s3 offset-s2">
             <a href="" class="btn waves-effect blue lighten-3">Enviar Solicitud   <i class="material-icons right">send</i></a>
             <empleo></empleo>
        </div>
    </div>
</div>
<footer-me></footer-me>

@endsection
