@extends('layouts.app')

@section('content')
  
    <menu-vista></menu-vista>

    <div class="container">
      <div id="demo">
        
        <center>
            <h1>{{trans('textos.internet_title')}}</h1>
            <h2>{{trans('textos.internet_subtitle')}}</h2>
        </center>   
        <div class="table-responsive-vertical shadow-z-1">

            <table id="table" class="table table-hover table-mc-light-blue striped">
                <thead>
                    <tr>
                    <th><b>PLAN</b></th>
                    <th style="text-align:center"><a class="btn-floating pulse green"><i class="material-icons">settings_input_antenna</i></a></th>
                    <th><b>{{trans('textos.internet_velocidad')}}</b></th>
                    <th><b>{{trans('textos.internet_support')}} 24/7</b></th>
                    <th><b>{{trans('textos.internet_price')}}</b></th>
                    <th><b>{{trans('textos.internet_contract')}}</b></th>
                    </tr>
                </thead>
                <tbody>
                    
                    <tr>
                    <td data-title="ID">{{trans('textos.internet_residential')}} 1Mb/1Mb</td>
                    <td data-title="Name" style="text-align:center"><a href="" style="color:blue"><b>{{trans('textos.internet_cobertura')}}</b></a></td>
                    <td data-title="Link"><img src="{{asset('images/prices/11.png')}}"></td>
                    <td data-title="Status"><span style="font-size:11px"><b>{{trans('textos.internet_dias')}}</b><br>{{trans('textos.internet_via')}}</span></td>
                    <td data-title="Precio"><b>$19.95</b></td>
                    <td data-title="Acction"> <modal-service tiposervicio="internet residencial"></modal-service></td>
                    </tr>
                    
                    <tr>
                    <td data-title="ID">{{trans('textos.internet_residential')}} 3Mb/1Mb</td>
                    <td data-title="Name" style="text-align:center"><a href="" style="color:blue"><b>{{trans('textos.internet_cobertura')}}</b></a></td>
                    <td data-title="Link"><img src="{{asset('images/prices/31.png')}}"></td>
                    <td data-title="Status"><span style="font-size:11px"><b>{{trans('textos.internet_dias')}}</b><br>{{trans('textos.internet_via')}}</span></td>
                    <td data-title="Precio"><b>$29.95</b></td>
                    <td data-title="Acction"> <modal-service tiposervicio="internet residencial"></modal-service></td>
                    </tr>
                    
                    <tr>
                    <td data-title="ID">{{trans('textos.internet_residential')}} 5Mb/1Mb</td>
                    <td data-title="Name" style="text-align:center"><a href="" style="color:blue"><b>{{trans('textos.internet_cobertura')}}</b></a></td>
                    <td data-title="Link"><img src="{{asset('images/prices/51.png')}}"></td>
                    <td data-title="Status"><span style="font-size:11px"><b>{{trans('textos.internet_dias')}}</b><br>{{trans('textos.internet_via')}}</span></td>
                    <td data-title="Precio"><b>$49.95</b></td>
                    <td data-title="Acction"> <modal-service tiposervicio="internet residencial"></modal-service></td>
                    </tr>

                    <tr>
                    <td data-title="ID">{{trans('textos.internet_residential')}} 10Mb/1Mb</td>
                    <td data-title="Name" style="text-align:center"><a href="" style="color:blue"><b>{{trans('textos.internet_cobertura')}}</b></a></td>
                    <td data-title="Link"><img src="{{asset('images/prices/102.png')}}"></td>
                    <td data-title="Status"><span style="font-size:11px"><b>{{trans('textos.internet_dias')}}</b><br>{{trans('textos.internet_via')}}</span></td>
                    <td data-title="Precio"><b>$69.95</b></td>
                    <td data-title="Acction"> <modal-service tiposervicio="internet residencial"></modal-service></td>
                    </tr>
                
                    <!-- Commercial plans -->

                    <tr>
                    <td data-title="ID">{{trans('textos.internet_comercial')}} 3Mb/1Mb</td>
                    <td data-title="Name" style="text-align:center"><a href="" style="color:blue"><b>{{trans('textos.internet_cobertura')}}</b></a></td>
                    <td data-title="Link"><img src="{{asset('images/prices/11.png')}}"></td>
                    <td data-title="Status"><span style="font-size:11px"><b>{{trans('textos.internet_dias')}}</b><br>{{trans('textos.internet_via')}}</span></td>
                    <td data-title="Precio"><b>$59.95</b></td>
                    <td data-title="Acction"> <modal-service tiposervicio="internet comercial"></modal-service></td>
                    </tr>
                    
                    <tr>
                    <td data-title="ID">{{trans('textos.internet_comercial')}} 5Mb/1Mb</td>
                    <td data-title="Name" style="text-align:center"><a href="" style="color:blue"><b>{{trans('textos.internet_cobertura')}}</b></a></td>
                    <td data-title="Link"><img src="{{asset('images/prices/31.png')}}"></td>
                    <td data-title="Status"><span style="font-size:11px"><b>{{trans('textos.internet_dias')}}</b><br>{{trans('textos.internet_via')}}</span></td>
                    <td data-title="Precio"><b>$79.95</b></td>
                    <td data-title="Acction"> <modal-service tiposervicio="internet comercial"></modal-service></td>
                    </tr>
                    
                    <tr>
                    <td data-title="ID">{{trans('textos.internet_comercial')}} 10Mb/2Mb</td>
                    <td data-title="Name" style="text-align:center"><a href="" style="color:blue"><b>{{trans('textos.internet_cobertura')}}</b></a></td>
                    <td data-title="Link"><img src="{{asset('images/prices/51.png')}}"></td>
                    <td data-title="Status"><span style="font-size:11px"><b>{{trans('textos.internet_dias')}}</b><br>{{trans('textos.internet_via')}}</span></td>
                    <td data-title="Precio"><b>$99.95</b></td>
                    <td data-title="Acction"> <modal-service tiposervicio="internet comercial"></modal-service></td>
                    </tr>

                    <tr>
                    <td data-title="ID">{{trans('textos.internet_comercial')}} 20Mb/5Mb</td>
                    <td data-title="Name" style="text-align:center"><a href="" style="color:blue"><b>{{trans('textos.internet_cobertura')}}</b></a></td>
                    <td data-title="Link"><img src="{{asset('images/prices/102.png')}}"></td>
                    <td data-title="Status"><span style="font-size:11px"><b>{{trans('textos.internet_dias')}}</b><br>{{trans('textos.internet_via')}}</span></td>
                    <td data-title="Precio"><b>$199.95</b></td>
                    <td data-title="Acction"> <modal-service tiposervicio="internet comercial"></modal-service></td>
                    </tr>
                
                    <!-- DEDICATED plans -->

                    <tr>
                    <td data-title="ID">{{trans('textos.internet_dedicado')}} 1Mb/1Mb</td>
                    <td data-title="Name" style="text-align:center"><a href="" style="color:blue"><b>{{trans('textos.internet_cobertura')}}</b></a></td>
                    <td data-title="Link"><img src="{{asset('images/prices/11.png')}}"></td>
                    <td data-title="Status"><span style="font-size:11px"><b>{{trans('textos.internet_dias')}}</b><br>{{trans('textos.internet_via')}}</span></td>
                    <td data-title="Precio"><b>CALL US</b></td>
                    <td data-title="Acction"> <modal-service tiposervicio="internet dedicado"></modal-service></td>
                    </tr>
                    
                    <tr>
                    <td data-title="ID">{{trans('textos.internet_dedicado')}} 3Mb/3Mb</td>
                    <td data-title="Name" style="text-align:center"><a href="" style="color:blue"><b>{{trans('textos.internet_cobertura')}}</b></a></td>
                    <td data-title="Link"><img src="{{asset('images/prices/31.png')}}"></td>
                    <td data-title="Status"><span style="font-size:11px"><b>{{trans('textos.internet_dias')}}</b><br>{{trans('textos.internet_via')}}</span></td>
                    <td data-title="Precio"><b>CALL US</b></td>
                    <td data-title="Acction"> <modal-service tiposervicio="internet dedicado"></modal-service></td>
                    </tr>
                    
                    <tr>
                    <td data-title="ID">{{trans('textos.internet_dedicado')}} 5Mb/5Mb</td>
                    <td data-title="Name" style="text-align:center"><a href="" style="color:blue"><b>{{trans('textos.internet_cobertura')}}</b></a></td>
                    <td data-title="Link"><img src="{{asset('images/prices/51.png')}}"></td>
                    <td data-title="Status"><span style="font-size:11px"><b>{{trans('textos.internet_dias')}}</b><br>{{trans('textos.internet_via')}}</span></td>
                    <td data-title="Precio"><b>CALL US</b></td>
                    <td data-title="Acction"> <modal-service tiposervicio="internet dedicado"></modal-service></td>
                    </tr>

                    <tr>
                    <td data-title="ID">{{trans('textos.internet_dedicado')}} 10Mb/10Mb</td>
                    <td data-title="Name" style="text-align:center"><a href="" style="color:blue"><b>{{trans('textos.internet_cobertura')}}</b></a></td>
                    <td data-title="Link"><img src="{{asset('images/prices/102.png')}}"></td>
                    <td data-title="Status"><span style="font-size:11px"><b>{{trans('textos.internet_dias')}}</b><br>{{trans('textos.internet_via')}}</span></td>
                    <td data-title="Precio"><b>CALL US</b></td>
                    <td data-title="Acction"> <modal-service tiposervicio="internet dedicado"></modal-service></td>
                    </tr>
                
                    <!-- Other plans -->

                </tbody>
            </table>
        </div>
     </div> 
    </div>
    <footer-me></footer-me>

@endsection


