@extends('layouts.app')

@section('content')
  <menu-vista></menu-vista>
    
    <div class="container">
       <div class="row">
            <div class="col s12">
                <center style="margin-top:4em;">
                    <img src="{{asset('images/logo-black.png')}}" alt="">
                </center>
                <p style="margin-top:4em">{{trans('textos.about_description')}}</p>
            </div>
       </div>
       <div class="row" style="margin-top:4em;">
            <div class="col s12">
                <div class="col s6">
                    <img src="{{asset('images/callcenter.png')}}" alt="" width="70%" style="border-radius:0.5em;">
                </div>
                <div class="col s6">
                    <h3>{{trans('textos.about_title')}}</h3>
                    <p>{{trans('textos.about_service')}}</p>
                </div>
            </div>
       </div>
       <div class="row" style="margin-top:2em;margin-bottom:4em;">
            <div class="col s12">
                <div class="col s6">
                    <h3 style="margin-top:2em;">{{trans('textos.about_title_personal')}}</h3>    
                    <p>{{trans('textos.about_personal')}}</p>
                </div>
                <div class="col s6">
                    <img src="{{asset('images/callcenter.png')}}" alt="" width="70%" style="border-radius:0.5em;">
                </div>
            </div>
       </div>

    </div>
  
  
  
    <footer-me></footer-me>
@endsection
