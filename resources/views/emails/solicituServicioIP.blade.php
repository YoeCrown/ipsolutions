<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Solicitud de servicio</title>
</head>
<body>

	<div style="background-color:black;padding:10px;">
		<img src="<?php echo 'http://ip-solutions.us/assets/images/logo.png'?>">
	</div><!--MAIL HEADER-->


<div style='background-color:#f4f4f4;padding:10px'>
	
	<p style="font-size:12px;">
		Solicitud de Servicio.
	</p>

	<h3> Datos del Solicitante:</h3>
	<ul>
		<li><span style='font-weight: bold;'>Recibido a las:</span> <?php echo date('H:i:s') ?></li>
		<li><span style='font-weight: bold;'>Nombre:</span> {{$data['nombre']}} </li>
		<li><span style='font-weight: bold;'>Email:</span> {{$data['correo']}} </li>
		<li><span style='font-weight: bold;'>Telefono:</span> {{$data['telefono']}} </li>
		<li><span style='font-weight: bold;'>Direccion:</span> {{$data['direccion']}}</li>
		<li><span style='font-weight: bold;'>Mensaje:</span> {{$data['mensaje']}}</li>
		<li><span style='font-weight: bold;'>Servicio:</span> {{$data['seleccionServicio']}}</li>
		<li><span style='font-weight: bold;'>Plan:</span> {{$data['seleccionPlan']}}</li>
	</ul>
	
	<hr>

</div> <!--MAIL CONTENT-->


<div style="background-color:black;padding:10px;">
	<p style="font-size:10px;color:white">
		Para más información llamenos al telefonos: 787-520-6000 o visitenos en
			<a href="http://ip-solutions.us/">www.ip-solutions.us</a>
		<br>
		Si usted no ha solicitado ninguno de nuestro servicios por favor haga clic <a href="#"><b>Aquí</b></a> para eliminar su email de nuestra lista de prospección
	</p> <!--MAIL FOTTER-->
</div>


</body>
</html>

		