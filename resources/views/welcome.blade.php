@extends('layouts.app')

@section('content')
  
    <menu-vista></menu-vista>
	<img-slider></img-slider>
    <services></services>
    <features></features>
    <streming></streming>
    <partner></partner>
    <footer-me></footer-me>

@endsection
