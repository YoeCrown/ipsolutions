
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
 
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


Vue.component('example',  require('./components/Example.vue'));

Vue.component('menu-vista',  require('./components/MenuVista.vue'));

Vue.component('img-slider',  require('./components/Slider.vue'));

Vue.component('services',  require('./components/Services.vue'));

Vue.component('partner',  require('./components/Partner.vue'));

Vue.component('footer-me',  require('./components/Footer.vue'));

Vue.component('features',  require('./components/Features.vue'));

Vue.component('streming',  require('./components/Streming.vue'));

Vue.component('modal-service',  require('./components/Modal.vue'));

Vue.component('tabs',  require('./components/Tabs.vue'));

Vue.component('empleo',  require('./components/Empleo.vue'));


import Vue from 'vue'
import { EventBus } from './event-bus'
import i18n from 'voo-i18n'
import { translations } from './translations.js'

Vue.use(i18n, translations)


const app = new Vue({
    el: '#app',
    data() {
        return {
        locale:  document.getElementsByTagName("html")[0].getAttribute("lang"), 
     }
    }
});


