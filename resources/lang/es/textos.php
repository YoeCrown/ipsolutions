<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'about_description' => 'Somos el ISP o "Internet Service Provider" preferido por los Boricuas, contamos con la mejor calificacion en el ranking de proveedores de internet y soluciones informaticas tambien somos reconocidos por la calidad de nuestros servicios de hosting transporte de datos y el servicio de internet mas rapido de la isla "IP FIBER", ademas contamos con la plataforma mas estable y actualizada, nos gusta mantenerno a la banguardia de la tecnologia para tener la seguridad de brindarte un verdadero servicio de calidad. Agora es facil solicitar tu servicio de internet , solo debes entrar a https://solicitar.ip-solutions.us y completar el siguiente formulario , nos pondremos en contacto contigo en los proximos 60 minutos.',
    'about_title' => 'SERVICIO',
    'about_service' => 'Contamos con una gran variedad de servicios, donde puedes adquirir el que desees y se acople a tus necesidades. De igual forma nuestros servicios cuentan con diferentes planes pensados para la comoda adquisicion de los clientes.',
    'about_title_personal'=> 'PERSONAL',
    'about_personal' => 'Nuestro personal esta altamente calificado para la realizacion de las operaciones tanto tecnicas como administrativas, nuetro equipo esta a la espera para atenderlo y brindarle orientacion en la necesidad que presente.',
    'support_title_questions' => 'Preguntas frecuentes',
    'support_questions_content' => 'A continuacion damos respuestas a las preguntas mas comunes realizadas por nuestros usuarios, esperamos que te sean de ayuda.',
    'support_title_inconvenient' => 'Inconvenientes y reclamos sobre tu servicio de internet',
    'support_inconvenient_content' => 'Si se presentara un inconveniente con tu servicio de internet, puedes usar nuestro sistema de tickets que estara disponible para tí las 24 horas del dia los 365 dias del año a tu disposicion y nuestro equipo de soporte estara atendiendo tu caso a la brevedad posible.',
    'support_title_coorde' => 'Obten tus coordenadas',
    'support_coorde_content' =>  'Este es un servicio gratuito que usa tu localizacion para obtener tus coordenadas, puedes usarlo para verificar la disponibilidad del servicio.',
    'support_title_calculate' => 'Calculadora para el servicio de internet',
    'support_calculate_content' => 'En caso que requieras conocer cuanto internet requieres en tu casa, esta es tu herramienta ideal, aqui verificaras cuanto internet necesitas para tu hogar y lograr el equilibrio y estabilidad al usar servicios de play estation, hulu, roku, apple tv, netflix ,etc.',
    'internet_title' => 'Lista de planes para servicios de Internet',
    'internet_subtitle' => 'Planes diseñados para ajustarse a cualquier bolsillo',
    'internet_velocidad' => 'VELOCIDAD',
    'internet_support' => 'SOPORTE',
    'internet_price' => 'PRECIO',
    'internet_contract' => 'CONTRATAR',
    'internet_residential' => 'RESIDENCIAL INALÁMBRICO',
    'internet_comercial' => 'COMERCIAL INALÁMBRICO',
    'internet_dedicado' => 'DEDICADO INALÁMBRICO',
    'internet_cobertura' => 'Verificar cobertura Ahora!',
    'internet_dias' => '365 dias de soporte',
    'internet_via' => 'Vía sistema de tickets',
    'voip_title' => 'Lista de planes para servicios de VoIP',
    'voip_subtitle' => 'Planes diseñados para ajustarse a cualquier bolsillo',
    'voip_support' => 'SOPORTE',
    'voip_price' => 'PRECIO',
    'voip_contract' => 'CONTRATAR',
    'voip_residential' => 'SERVICIO VOIP RESIDENCIAL',
    'voip_measured' => 'SERVICIO VOIP MEDIDO',
    'voip_commercial' => 'SERVICIO VOIP COMERCIAL',
    'voip_regular' => 'SERVICIO VOIP REGULAR',
    'voip_dias' => '365 dias de soporte',
    'voip_via' => 'Vía sistema de tickets',
    'voip_residential_price' => 'Ilimitado $24.95 por línea',
    'voip_measured_price' => '$9.95 por linea',
    'voip_commercial_price' => 'Ilimitado $44.95 por línea',
    'voip_regular_price' => '$12.95 por línea y canales adicionales ',
    'voip_regular_price_b' => 'a $9.95 por línea',


];
