<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'about_description' => 'We are the IPS or "Internet Service Provider" preferred by the Boricuas, we have the best rating in the ranking of Internet providers and computer solutions are also recognized by the quality of our services of hosting data transport and the fastest internet service Of the island "IP FIBER", we also have the most stable and updated platform, we like to keep the technology banguard to have the security of providing you with a real quality service. Now it is easy to request your internet service, you just have to enter https://solicitar.ip-solutions.us and complete the following form, we will get back to you in the next 60 minutes.',
    'about_title' => 'SERVICE',
    'about_service'=> 'We have a wide variety of services, where you can get the one you want and fit your needs. Likewise our services have different plans designed for the comfortable acquisition of customers.',
    'about_title_personal'=> 'PERSONAL',
    'about_personal' => 'Our staff is highly qualified for the execution of technical and administrative operations, our team is waiting to assist you and provide guidance on the need you present.',
    'support_title_questions' => 'Frequent questions',
    'support_questions_content' => 'Here are answers to the most common questions asked by our users, we hope they will help you.',
    'support_title_inconvenient' => 'Disadvantages and complaints about your internet service',
    'support_inconvenient_content' => 'If you have a problem with your internet service, you can use our ticket system that will be available to you 24 hours a day, 365 days a year at your disposal and our support team will be attending your case as soon as possible.',
    'support_title_coorde' => 'Get your coordinates',
    'support_coorde_content' => 'This is a free service that uses your location to get your coordinates, you can use it to verify the availability of the service.',
    'support_title_calculate' => 'Calculator for internet service',
    'support_calculate_content' => 'In case you need to know how much internet you need in your house, this is your ideal tool, here you will check how much internet you need for your home and achieve balance and stability when using services of play, hulu, roku, apple tv, netflix, etc.',
    'internet_title' => 'List of plans for Internet services',
    'internet_subtitle' => 'Plans designed to fit any pocket',
    'internet_velocidad' => 'SPEED',
    'internet_support' => 'SUPPORT',
    'internet_price' => 'PRICE',
    'internet_contract' => 'CONTRACT',
    'internet_residential' => 'RESIDENTIAL WIRELESS ',
    'internet_comercial' => 'COMMERCIAL WIRELESS',
    'internet_dedicado' => 'DEDICATED WIRELESS',
    'internet_cobertura' => 'Check cover Now!',
    'internet_dias' => '365 days of support',
    'internet_via' => 'Via ticket system',
    'voip_title' => 'List of plans for VoIP services',
    'voip_subtitle' => 'Plans designed to fit any pocket',
    'voip_support' => 'SUPPORT',
    'voip_price' => 'PRICE',
    'voip_contract' => 'CONTRACT',
    'voip_residential' => 'VOIP RESIDENTIAL SERVICE',
    'voip_measured' => 'VOIP MEASURED SERVICE',
    'voip_commercial' => 'VOIP COMMERCIAL SERVICE',
    'voip_regular' => 'VOIP REGULAR SERVICE',
    'voip_dias' => '365 days of support',
    'voip_via' => 'Via ticket system',
    'voip_residential_price' => 'Unlimited $24.95 per line',
    'voip_measured_price' => '$9.95 per line',
    'voip_commercial_price' => 'Unlimited $44.95 per line',
    'voip_regular_price' => '$12.95 per line and additional channels ',
    'voip_regular_price_b' => 'to $9.95 per line',



];
